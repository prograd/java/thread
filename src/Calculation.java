public class Calculation implements Runnable{
    private int num1;
    private int num2;
    private int sum;
    Thread threadObj1;
    public Calculation(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
        this.sum = 0;
        threadObj1=new Thread(this,"addition thread");
        System.out.println("Thread started");
        threadObj1.start();
    }
    public void run(){
        System.out.println("thread in running state");
        this.sum=this.num1+this.num2;
        System.out.println(this.sum);
}

}
