public class SampleProgram implements Runnable{
    Thread thread1;
    String name;

    public SampleProgram(String name) {
        this.name = name;
        thread1=new Thread(this,name);
        thread1.start();
    }

    @Override
    public void run() {
        System.out.println("thread running");
    }

}
